﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Monetization;
using System.Collections;

// Класс для работы с рекламой за вознаграждение
[RequireComponent(typeof(Button))]
public class UnityAdsButton : MonoBehaviour
{

    public string placementId = "rewardedVideo";
    private Button adButton;
    public GameObject Bulit, Gom, CandyStick;
    public AudioSource Source;
    public GameObject countDown, pauseBtn;
    public AudioClip countDownSong;
    public DelayedStart DS;

#if UNITY_IOS
   private string gameId = "1234567";
#elif UNITY_ANDROID
    private string gameId = "3948115";
#endif

    void Start()
    {
        adButton = GetComponent<Button>();
        if (adButton)
        {
            adButton.onClick.AddListener(ShowAd);
        }

        if (Monetization.isSupported)
        {
            Monetization.Initialize(gameId, false);
        }
    }

    void Update()
    {
        if (adButton)
        {
            adButton.interactable = Monetization.IsReady(placementId);
        }
    }

    void ShowAd()
    {
        ShowAdCallbacks options = new ShowAdCallbacks();
        options.finishCallback = HandleShowResult;
        ShowAdPlacementContent ad = Monetization.GetPlacementContent(placementId) as ShowAdPlacementContent;
        ad.Show(options);
    }

    void HandleShowResult(ShowResult result)
    {
        if (result == ShowResult.Finished)
        {
            Debug.Log("Ok");
            float randomX = Random.Range(-9f, 9f);
            Bulit.transform.position = new Vector3(randomX, 18f, -10.5f);
            Bulit.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 0f);
            float randomAngle = Random.Range(-45f, 45f);
            CandyStick.transform.rotation = Quaternion.Euler(0f, 0f, randomAngle);
            Gom.SetActive(false);
            DS.StartToDely();
            Source.PlayOneShot(countDownSong);
            pauseBtn.SetActive(true);

        }
        else if (result == ShowResult.Skipped)
        {
            Debug.LogWarning("The player skipped the video - DO NOT REWARD!");
        }
        else if (result == ShowResult.Failed)
        {
            Debug.LogError("Video failed to show");
        }
    }
}