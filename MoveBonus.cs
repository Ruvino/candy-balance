﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Класс, который отвечает за движения бонусных конфет
public class MoveBonus : MonoBehaviour
{
    public Transform[] points;
    public float speed = 10f;
    int i;

    void Start()
    {
        i = Random.Range(0, points.Length); 
        transform.position = new Vector3(points[i].position.x, points[i].position.y, transform.position.z);
    }

    // Бонусные конфеты передвигаеются от одной точки до другой
    void Update()
    { 
       transform.position = Vector3.MoveTowards(transform.position, points[i].position, speed * Time.deltaTime);
        if (transform.position == points[i].position)
            i = Random.Range(0, points.Length); 
    }

}
