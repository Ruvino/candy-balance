﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;

// Класс для работы в главном меню
public class Menu : MonoBehaviour
{
    // Запуск игры
    public void LoadLevel()
    {
        SceneManager.LoadScene(1);
    }
     
    // Выход из приложения
    public void ExitGame()
    {
        Application.Quit();
    }
}
