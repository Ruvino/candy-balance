﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


// Класс, который привязан к бонусной конфете, которая добавляет защитные дуги, чтобы конфета не упала.
public class AddCane : MonoBehaviour, IPointerClickHandler
{
    public GameObject CaneCandy;
    public GameObject[] CaneCandyChild;
    public SpriteRenderer[] sr;
    SpriteRenderer Invisible;
    public AudioSource source;
    public AudioClip clip;

    // Метод при нажатии на конфету
    public void OnPointerClick(PointerEventData eventData)
    {
        source.PlayOneShot(clip);
        PreStart();
        StartCoroutine(DisableCane());
        StartCoroutine(InvisBonus());
    }

    // Карутина, в которой активируются защитные дуги и постепенно исчезают.
    IEnumerator DisableCane()
    {
        CaneCandy.SetActive(true);
        CaneCandyChild[0].SetActive(true);
        CaneCandyChild[1].SetActive(true);
        yield return new WaitForSeconds(0.1f * Time.deltaTime);
      
                sr[0].color = new Color(sr[0].color.r, sr[0].color.g, sr[0].color.b, sr[0].color.a - 0.05f * Time.deltaTime);
                sr[1].color = sr[0].color;
                if (sr[1].color.a <= 0f)
                {
                    CaneCandy.SetActive(false);
                    CaneCandyChild[0].SetActive(false);
                    CaneCandyChild[1].SetActive(false);
        }
                else
                {
                    sr[0].color = new Color(sr[0].color.r, sr[0].color.g, sr[0].color.b, sr[0].color.a - 0.05f * Time.deltaTime);
                    sr[1].color = sr[0].color;
                    StartCoroutine(DisableCane());
                }
        
     }

    // Карутина, в которой исчезает бонусная конфета, чтобы её нельзя было бесконечно нажимать.
    IEnumerator InvisBonus()
    {
        Invisible.color = new Color(Invisible.color.r, Invisible.color.g, Invisible.color.b, 0f);
        yield return new WaitForSeconds(30f);
        Invisible.color = new Color(Invisible.color.r, Invisible.color.g, Invisible.color.b, 1f);
    }

    // Подготовительные данные в которой инициализируются компоненты [для оптимизации]
    void PreStart()
    {
        sr[0] = CaneCandyChild[0].GetComponent<SpriteRenderer>();
        sr[1] = CaneCandyChild[1].GetComponent<SpriteRenderer>();

        sr[0].color = new Color(sr[0].color.r, sr[0].color.g, sr[0].color.b, 1f);
        sr[1].color = sr[0].color;

        Invisible = GetComponent<SpriteRenderer>();
    }
}