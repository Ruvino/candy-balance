﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;

// Класс, который привязан к главной конфете, которой надо балансировать
public class Bulit : MonoBehaviour
{
    public GameObject SimpleTheEnd, RewardedTheEnd;
    public AudioSource source;
    public AudioClip clip;
    public int DeadCount;
    public bool itFirstDead = true;
    public GameObject PauseBtn;
    public UnityAdsPlacement unityAds;

    private void Start()
    {
        // Инициализируем "Счётчик смертей"
        if (!PlayerPrefs.HasKey("DeadCount"))
        {
            PlayerPrefs.SetInt("DeadCount", DeadCount);
        }
        else
        {
            DeadCount = PlayerPrefs.GetInt("DeadCount");
        }
        // Устанавлием рандомную позицию, с которой будет падать конфета
        float randomX = Random.Range(-9f, 9f);
        transform.position = new Vector3(randomX, transform.position.y, transform.position.z);
    }

    // При падении конфета пересекает коллайдер и игра завершается
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("DeathCollider"))
        {
            GameOver();
        }
    }
 
    // Скрипт завершения игры
    public void GameOver()
    {
        // Увеличиваем счётчик смертей, замораживаем время. Если смертей становится 5, то включается реклама, которую можно пропустить через 10 секунд.
        DeadCount += 1;
        PlayerPrefs.SetInt("DeadCount", DeadCount);
        Time.timeScale = 0f;
        if (PlayerPrefs.GetInt("DeadCount") > 4)
        {
            unityAds.ShowAd();
        }
        // Если это первое поражение, то  открываем окно с возможностью просмотра рекламы за вознограждение
        if (itFirstDead)
        {
            RewardedTheEnd.SetActive(true);
            PauseBtn.SetActive(false);
            source.PlayOneShot(clip);
            itFirstDead = false;
            return;
        }
        // Если игрок просмотрел рекламу, то уже окончательное поражение
        if (!itFirstDead)
        {
            SimpleTheEnd.SetActive(true);
            PauseBtn.SetActive(false);
            source.PlayOneShot(clip);
        }
        
    }
}
