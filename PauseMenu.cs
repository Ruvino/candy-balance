﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Класс, для работы в меню паузы
public class PauseMenu : MonoBehaviour
{
    public GameObject PauseMenuPanel, PauseBtn;

    // Пауза включена
    public  void PauseOn()
    {
        PauseMenuPanel.SetActive(true);
        Time.timeScale = 0f;
        PauseBtn.SetActive(false);

    }

    // Пауза выключена
    public void PauseOff()
    {
        PauseMenuPanel.SetActive(false);
        Time.timeScale = 1f;
        PauseBtn.SetActive(true);
    }

    // Выйти в гланвое меню
    public void ExitToMainMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}
