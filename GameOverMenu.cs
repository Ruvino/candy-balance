﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Класс для работы в меню окончания игры
public class GameOverMenu : MonoBehaviour
{
    // Перезапуск уровня
    public void RestartLevel()
    {
        SceneManager.LoadScene("Level");
    }

    // Выход в главное меню
    public void ExitToMainMenu()
    {
        SceneManager.LoadScene(0);
    }
}
