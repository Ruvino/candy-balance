﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Метод, который устанавливает трость под случайным углом в начале игры
public class CandyStick : MonoBehaviour
{
    void Start()
    {
        float randomAngle = Random.Range(-45f, 45f);
        transform.rotation = Quaternion.Euler(0f, 0f, randomAngle);
    }
}
