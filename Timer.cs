﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Класс который фиксирует время игры
public class Timer : MonoBehaviour
{
    public float PlayTime = 0f;
    public float highScore = 0f;
    string highScoreKey = "HighScore";
    public Text PlayTimeText;

    void Start()
    {
        highScore = PlayerPrefs.GetFloat(highScoreKey, 0f);
    }

    void Update()
    {
        PlayTime += Time.deltaTime;

        if (PlayTime <= 60)
        {
            PlayTimeText.text = PlayTime.ToString("F3") + "s";
        }
        else
        {
            PlayTimeText.text = ((int)PlayTime / 60).ToString() + "m:" + ((int)PlayTime - ((int)PlayTime / 60) * 60).ToString("D2") + "s";
        }

    }
    
    // При выходе из уровня сравниваются показатели рекорда и времени игры, если время игры больше, то показатель записывается в рекорд
    void OnDisable()
    {
        if (PlayTime > highScore)
        {
            PlayerPrefs.SetFloat(highScoreKey, PlayTime);
            PlayerPrefs.Save();
        }
    }
}
