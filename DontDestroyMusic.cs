﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Метод, который воспроизводит музыку при запуске игры и при переходе по сценам она продолжает играть
public class DontDestroyMusic : MonoBehaviour
{
    void Awake()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("MainMusic");

        if (objs.Length > 1)
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);
    }
}