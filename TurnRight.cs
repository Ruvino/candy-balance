﻿using UnityEngine;

// Класс для поворота трости в правую сторону
public class TurnRight : MonoBehaviour
{
    float zAngleRight = 300f;
    bool isButtonPressed = false;

    // Проверка на зажатие кнопки
    void FixedUpdate()
    {
        if (isButtonPressed)
        {
            transform.Rotate(0f, 0f, zAngleRight * Time.deltaTime, Space.Self);
        }
    }

    // Метод при нажатии на кнопку поворота направо
    public void onPointerDownButton()
    {
        isButtonPressed = true;
    }

    // Метод при прекращении нажатия на кнопку поворота направо
    public void onPointerUpButton()
    {
        isButtonPressed = false;
    }
}