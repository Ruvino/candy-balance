﻿using UnityEngine;

// Класс для поворота трости в левую сторону
public class TurnLeft: MonoBehaviour
{
    float zAngleLeft = -300f;
    bool isButtonPressed = false;

    // Проверка на зажатие кнопки
    void FixedUpdate()
    {
        if (isButtonPressed)
        {
            transform.Rotate(0f, 0f, zAngleLeft * Time.deltaTime, Space.Self);
        }
    }
    
    // Метод при нажатии на кнопку поворота налево
    public void onPointerDownButton()
    {
        isButtonPressed = true;
    }

    // Метод при прекращении нажатия на кнопку поворота налево
    public void onPointerUpButton()
    {
        isButtonPressed = false;
    }
}