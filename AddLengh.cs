﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


// Класс, который привязан к бонусной конфете, которая увеличивает длинну "Трости"
public class AddLengh : MonoBehaviour, IPointerClickHandler
{
    public GameObject CaneCandy;
    public Transform tr;
    SpriteRenderer Invisible;
    public AudioSource source;
    public AudioClip clip;

    // Метод при нажатии на конфету
    public void OnPointerClick(PointerEventData eventData)
    {
        source.PlayOneShot(clip);
        preStart();
        StartCoroutine(DisableLengh());
        StartCoroutine(InvisBonus());
    }

    // Карутина, в которой увеличивается длина трости и постепенно уменьшается.
    IEnumerator DisableLengh()
    {
        yield return new WaitForSeconds(0.08f * Time.deltaTime);

        tr.localScale = new Vector3(tr.localScale.x - 0.05f * Time.deltaTime, tr.localScale.y, tr.localScale.z);

        if (tr.localScale.x <= 5f)
        {
            tr.localScale = new Vector3(5f, 5f, 5f);
        }
        else
        {
            tr.localScale = new Vector3(tr.localScale.x - 0.05f * Time.deltaTime, tr.localScale.y, tr.localScale.z);
            StartCoroutine(DisableLengh());
        }
    }

    // Карутина, в которой исчезает бонусная конфета, чтобы её нельзя было бесконечно нажимать.
    IEnumerator InvisBonus()
    {
        Invisible.color = new Color(Invisible.color.r, Invisible.color.g, Invisible.color.b, 0f);
        yield return new WaitForSeconds(30f);
        Invisible.color = new Color(Invisible.color.r, Invisible.color.g, Invisible.color.b, 1f);
    }

    // Подготовительные данные в которой инициализируются компоненты [для оптимизации]
    void preStart()
    {
        tr = CaneCandy.GetComponent<Transform>();
        tr.localScale = new Vector3(8f, 5f, 5f);
        Invisible = GetComponent<SpriteRenderer>();
    }
}