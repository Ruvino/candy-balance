﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Метод отложенного запука, чтобы уровень запускался через 3 секунды, после запуска сцены
public class DelayedStart : MonoBehaviour
{
    public GameObject countDown, timer, PauseBtn;
    public AudioClip countDownSong;

    void Start()
    {
        StartCoroutine(StartDelay());
    }

    IEnumerator StartDelay()
    {
        PauseBtn.SetActive(false);
        countDown.gameObject.SetActive(true);
        Time.timeScale = 0f;
        float pauseTime = Time.realtimeSinceStartup + 3f;
        while (Time.realtimeSinceStartup < pauseTime)
        {
            yield return 0;
        }
        countDown.gameObject.SetActive(false);
        timer.gameObject.SetActive(true);
        PauseBtn.SetActive(true);
        Time.timeScale = 1f;
    }

    public void StartToDely()
    {
        StartCoroutine(StartDelay());
    }
}
