﻿using UnityEngine;
using UnityEngine.Monetization;

// Класс для инициализации рекламы при запуске игры
public class UnityAdsScript : MonoBehaviour
{

    string gameId = "3948115";
    bool testMode = false;

    void Start()
    {
        Monetization.Initialize(gameId, testMode);
    }
}
