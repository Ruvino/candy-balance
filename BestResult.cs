﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


// Метод, который выводить в главное меню лучшее время.
public class BestResult : MonoBehaviour
{
    public float BestResultTime;
    public Text BestResultText;
    string highScoreKey = "HighScore";

    void Start()
    {
        BestResultTime = PlayerPrefs.GetFloat(highScoreKey, 0f);
        if (BestResultTime <= 60)
        {
            BestResultText.text = "Best result: " + BestResultTime.ToString("F3") + "s";
        }
        else
        {
            BestResultText.text = $"Best result: {((int)BestResultTime / 60).ToString()} m: {((int)BestResultTime - ((int)BestResultTime / 60) * 60).ToString("D2")} s";
        }
    }
}
